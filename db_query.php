<?php
$host = "localhost";
$user = "test";
$pass = "t3st3r123";
$db = "test";

$l = mysqli_connect($host, $user, $pass, $db);
mysqli_query($l, "SET CHARACTER SET UTF8") or
    die("Error, ei saa andmebaasi charsetti seatud");


$sql = "SELECT * FROM aseiman_hinne";

$result = $l->query($sql);
if (!$result)
	echo mysqli_error($l);


$sum = 0;
$arv = 0;
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
		$sum = $sum + $row['arv'] * $row['hinne'];
		$arv = $arv + $row['arv'];
	}
}
echo round($sum / $arv, 2);
mysqli_close($l);
?>
