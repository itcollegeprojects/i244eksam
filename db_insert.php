<?php
// Attempt MySQL server connection.
$link = mysqli_connect("localhost", "test", "t3st3r123", "test");
 


// Check connection
if($link === false){
	die("ERROR: Could not connect. " . mysqli_connect_error());
}



// Set UTF-8 for database connection
if (!$link->set_charset("utf8")) {
	printf("Error loading character set utf8: %s\n", $link->error);
	exit();
}



// Escape user inputs for security
$hinne = mysqli_real_escape_string($link, $_POST['hinne']);



// Validate user inputs
if (empty($hinne)) {
	$hinne_error = 'Hinne sisestamata!';
} else {
	$hinne = test_input($hinne);
	if ($hinne == FALSE) {
		$hinne_error = 'Hinne vigane! Sisesta palun arv vahemikus 1-5.';
	}
}



// Attempt insert query execution
if (empty($hinne_error)) {
	$sql = "UPDATE aseiman_hinne SET arv = arv + 1 WHERE hinne = '$hinne';";
	$viimane_hinne = $hinne;
	$hinne_teade = 'Sinu hinne on antud! Sa hindasid seda veebilehte hindega: ' .$hinne;
	if (mysqli_query($link, $sql)) {
		include('index.php');
	} else{
		echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
	}
} else {
	include('index.php');
}


 
// Close connection
mysqli_close($link);



// Functions
function test_input($field) {
    $field = filter_var(trim($field), FILTER_SANITIZE_STRING);
    if (ctype_digit($field) && intval($field) > 0 && intval($field) < 6) {
        return intval($field);
    } else {
        return FALSE;
    }
}
?>
