﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="author" content="Andrus Seiman" >
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>PHP-MySQL</title>
	<style type="text/css">
		.error{ color: red; }
		.success{ color: green; }
	</style>
	<script src="my_scripts.js"></script>
</head>


<body>

<h1>i244 Eksam: Andrus Seiman</h1>

<h2>Eksami ülesanne:</h2>
<p>Loo lehekülje hindamise võimalus (skaalal 1-5 näiteks). Kuva välja leheküljele antud keskmist hinnet. Andmed salvesta andmebaasi.</p>
<p>Lahendust mõeldes eelda, et kasutaja brauseris on lubatud nii Javascript kui ka küpsised.</p>

<h2>Anna oma hinne:</h2>
<?php include('reset_vars.php'); ?>
<p>Selleks kliki tähekesel:</p>
<img id="img1" onClick="lisa_hinne(1)" src="star.svg" style="width:20px; height:20px; margin:2px 2px 2px 2px;" alt="">
<img id="img2" onClick="lisa_hinne(2)" src="star.svg" style="width:20px; height:20px; margin:2px 2px 2px 2px;" alt="">
<img id="img3" onClick="lisa_hinne(3)" src="star.svg" style="width:20px; height:20px; margin:2px 2px 2px 2px;" alt="">
<img id="img4" onClick="lisa_hinne(4)" src="star.svg" style="width:20px; height:20px; margin:2px 2px 2px 2px;" alt="">
<img id="img5" onClick="lisa_hinne(5)" src="star.svg" style="width:20px; height:20px; margin:2px 2px 2px 2px;" alt="">

<p>Või sisesta number vahemikus 1 kuni 5.</p>
<form action="db_insert.php" method="post" accept-charset="UTF-8">
	<input type="text" name="hinne" value="<?php echo $hinne; ?>">
	<input class="5" type="submit" value="OK">
	<span class="error"><?php echo $hinne_error; ?></span>
	<span class="teade"><?php echo $hinne_teade; ?></span>
</form>

<h2>Seni antud keskmine hinne on:</h2>
<?php include('db_query.php'); ?>

</body>
</html>
